const webpack = require('webpack');
const path = require('path');

const rootPath = path.join(__dirname, '..', 'client', 'app');

const webpackConfig = {
    entry: {
        app: path.join(rootPath, 'index.js')
    },

    output: {
        filename: './client/public/js/[name].js'
    },

    resolve: {
        extensions: ['.js', '.vue', '.scss'],

        alias: {
            'vue$': 'vue/dist/vue.esm.js',
            'utils': path.join(rootPath, 'utils')
        }
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: [/node_modules/],
                options: {
                    presets: ["env"]
                }
            },

            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },

            {
                test: /\.scss$/,
                use: [{
                    loader: 'style-loader'
                }, {
                    loader: 'css-loader'
                }, {
                    loader: 'sass-loader'
                }]
            }
        ]
    }
}

module.exports = webpackConfig;