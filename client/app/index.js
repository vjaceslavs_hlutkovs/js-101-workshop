import '../scss/style';
import Vue from 'vue';
import App from './component/App';

new Vue({
    el: '#app',

    components: {
        App
    }
});