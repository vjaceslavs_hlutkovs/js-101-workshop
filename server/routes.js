const express = require('express');
const homeController = require('controller/home');

module.exports = (() => {
    const router = express.Router();

    router.get('/', homeController.index);
    router.get('/ping', homeController.ping);

    return router;
})();