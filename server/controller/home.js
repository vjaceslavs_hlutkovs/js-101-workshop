const index = (req, res) => {
    return res.render('index', {
        welcome: 'Hello world'
    });
}

const ping = (req, res) => {
    return res.json({ pong: true });
}

module.exports = {
    index,
    ping
}