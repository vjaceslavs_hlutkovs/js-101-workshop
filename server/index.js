require('module-alias/register');

const path = require('path');
const express = require('express');
const routes = require('./routes');

const app = express();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '..', 'client', 'views'));
app.use(express.static('client/public'));

app.use(express.json());
app.use(routes);

app.listen(3000, () => { console.log('Listening on 3000') });